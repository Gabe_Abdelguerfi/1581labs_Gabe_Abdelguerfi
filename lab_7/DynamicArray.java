/**
 * A dynamically resizable array modeled on Java's
 * Arraylist.
 * @author Gabe Abdelguerfi
 * @version 17 October 2016
 */
 import java.util.Arrays;
public class DynamicArray
{
    private String[] array;
    private int numberOfElements;
    private int currentIndex;
    
    public DynamicArray() {
        this.array = new String[10];
        this.numberOfElements = 0;
        this.currentIndex = 0;
    } // end default constructor
    
    public DynamicArray(int size) {
        this.array = new String[size];
        this.numberOfElements = 0;
        this.currentIndex = 0;
    } // end overloaded constructor
    
    public DynamicArray(String[] strings) {
        this.array = Arrays.copyOf(strings, strings.length);
        this.numberOfElements = strings.length;
        this.currentIndex = strings.length;
    } // end overloaded constructor
    
    public void add(String newString) {
        if (this.array.length == this.currentIndex) {
            this.expandArray();
        }
        this.array[currentIndex] = newString;
        this.numberOfElements++;
        this.currentIndex++;
    } // end method add
    
    private void expandArray() {
        int newLength = this.array.length * 3 / 2;
        String[] newArray = new String[newLength];
        
        for (int i = 0; i < this.array.length; i ++) {
            newArray[i] = this.array[i];
        }
        
        this.array = newArray;
    } // end expandArray
    
    public String remove(int index) {
        String temp = this.array[index];
        
        for (int i = index; i < this.numberOfElements - 1; i++) {
            this.array[i] = this.array[i + 1];
        }
        
        this.currentIndex--;
        this.numberOfElements--;
        return temp;
    } //end method remove
    
    public String get(int index) {
        return this.array[index];
    } // end method get
    
    public boolean isEmpty() {
        if (this.numberOfElements == 0) {
            return true;
        }
        
        else {
            return false;
        }
    } //end method isEmpty
    
    public int sizeOf() {
        return this.numberOfElements;
    } //end method sizeOf
} // end class DynamicArray