/**
 *A class that manipulates two arrays.
 * @author Gabriel Abdelguerfi
 * @revised 19 September 2016
*/
import java.util.Random;

public class Exercise_One
{   
    public static void main(String[] args)
    {   
        Random randomNumb = new Random();
        int[] myInts = new int[10];
        
        for(int index = 0; index < myInts.length; index++ )
        {
            myInts[index] = randomNumb.nextInt();
        }
        for(int index = 0; index < myInts.length; index++){
            System.out.println(myInts[index]);
        }
        String[] myMonths = {"January", "February", "March", "April",
                               "May", "June", "June", "July", "August",
                               "September", "October", "November", "December"};
        
        System.out.println("A month: "+ myMonths[randomNumb.nextInt(myMonths.length)]);
    } //closing main method
} // closing class