/**
 * A program(using methods) that converts Fahrenheit to Celsius and vice versa. This 
 * version of the program is poorly constructed with all of the code
 * in the main method. 
 *
 * Remember: 
 * 		tempCelsius = (tempFahr - 32) / 1.8 
 * 		tempFahr = tempCelsius * 1.8 + 32
 *
 * Author: Gabriel Abdelguerfi
 * Revised: September 12 at 11:00
 * Assignment: Laboratory 4 - Methods
 * Class: GoodTempConverter
 */ 

import java.util.Scanner; 

public class GoodTempConverter {
	static int userChoice; 
	static double tempCelsius; 
	static double tempFahr; 
	static Scanner input = new Scanner(System.in);
	public static void main(String[] args) {
		while (userChoice != 3) {
		printMenu();
		executeChoice();
		}
	}
		public static void printMenu() {
		// Loop until user opts to quit program. 
			// Print the user's options. 
			System.out.println("Please select one of the following options: "); 
			System.out.println("	1. Convert Fahrenheit to Celsius; "); 
			System.out.println("	2. Convert Celsius to Fahrenheit; or "); 
			System.out.println("	3. Quit "); 

			} //closes printMenu(); method
			public static void convertFtoC(){
			// User wants to convert Fahrenheit to Celsius. 
				System.out.printf("Please enter the temperature in Fahrenheit: "); 
				tempFahr = input.nextDouble(); 
				tempCelsius = (tempFahr - 32) / 1.8; 
				System.out.printf("%.2f degrees Fahrenheit is equal to %.2f degrees Celsius.%n%n",
					tempFahr, tempCelsius); 
				if (userChoice == 3) {
					System.out.println("Thanks! Have a nice day!");  
				} 
			}
			
			public static void convertCtoF(){
			// User wants to convert Celsius to Fahrenheit. 
				System.out.printf("Please enter the temperature in Celsius: "); 
				tempCelsius = input.nextDouble(); 
				tempFahr = tempCelsius * 1.8 + 32; 
				System.out.printf("%.2f degrees Celsius is equal to %.2f degrees Fahrenheit.%n%n", 
					tempCelsius, tempFahr); 
			}
			public static void executeChoice(){
				// Gets user's choice
				userChoice = input.nextInt(); 
				if(userChoice == 1){
					convertFtoC();
				}
				else if(userChoice == 2){
					convertCtoF();
				}
				else if(userChoice == 3)
					quitProgram();
				else{
					invalidInput();
				}
				return;
			}
			public static void quitProgram(){
				System.out.println("Thanks! Have a nice day!");  
			} 
			public static void invalidInput(){
				// User entered invalid input. 
				System.out.println("Invalid input. Please enter only 1, 2, or 3."); 
			} 
} // end class