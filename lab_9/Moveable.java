/**
* An interface that stipulates that an instance
* of implementing classes can be moved horizontally
* or vertically in the coordiante plane.
* @author Gabe Abdelguerfi
* @version 7 Nov 16
*/
public interface Moveable{
    void moveVertical(double shift);
    void moveHorizontal(double shift);
} // end interface Moveable