public class Runner
{
    public static void main(String[] args)
    {
        Point2D vertOne = new Point2D(0.0, 0.0);
        Point2D vertTwo = new Point2D(0.0, 2.0);
        Point2D vertThree = new Point2D(4.0, 0.0);
        Triangle myTriangle = new Triangle(vertOne, vertTwo, vertThree);
        System.out.println("Area" + myTriangle.area());
        Shape[] myShapes = new Shape[2];
        myShapes[0] = myTriangle;
        
        Point2D anotherVert1 = new Point2D(1.0,1.0);
        Point2D anotherVert2 = new Point2D(5.0, 1.0);
        Point2D anotherVert3 = new Point2D(1.0, 3.0);
        Triangle traingleTwo = new Triangle(anotherVert1, anotherVert2, anotherVert3);
        
        myShapes[1] = traingleTwo;
        
        for(Shape shape : myShapes) {
            System.out.println(shape.toString());
            System.out.println(shape.area());
        }
        
        System.out.println();
        System.out.println(traingleTwo);
        traingleTwo.moveVertical(3.5);
        System.out.println(traingleTwo);
    } //end main method
} //end class runner