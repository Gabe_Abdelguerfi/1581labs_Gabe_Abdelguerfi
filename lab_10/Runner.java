/**
 * Sandbox for working with Fractions and exceptions for
 * Lab 10.
 *
 * Author: Gabe Abdelguerfi
 * Revised: 14 November 2016
 * Assignment: Lab 10 - Exceptions
 * Class: Runner
 */ 
 
import java.util.Scanner; 

public class Runner {
	public static void main(String[] args) {
		// Create Scanner instance to get user input. 
		Scanner input = new Scanner(System.in); 
		
		// Get values for first Fraction and create it. 
		System.out.printf("Please enter the dividend Fraction's numerator: "); 
		int firstNum = input.nextInt(); 
		System.out.printf("Please enter the dividend Fraction's denominator: "); 
		int firstDen = input.nextInt(); 
		Fraction dividend = new Fraction(firstNum, firstDen); 
		
		boolean successfulDivision = false;
		while(!successfulDivision) {

			// Get values for second Fraction and create it. 
			System.out.printf("Please enter the divisor Fraction's numerator: "); 
			int secondNum = input.nextInt(); 
			System.out.printf("Please enter the divisor Fraction's denominator: "); 
			int secondDen = input.nextInt(); 
			Fraction divisor = new Fraction(secondNum, secondDen); 
			
			// Calculate quotient. Print results. 
			try{
				Fraction quotient = dividend.divide(divisor); 
				System.out.println("Their quotient is: " + quotient);
				successfulDivision = true;
				
			}
			catch (IllegalArgumentException e){
				System.out.println();
				System.out.println("Division by zero not permitted.");
				System.out.println("Numerator of divisor must be nonzero.");
				System.out.println();
				System.out.println("Please re-enter values for divisor fraction.");
				System.out.println();
				e.printStackTrace();
				
			}
		} //end while loop
	} // end method main
} // end class Runner