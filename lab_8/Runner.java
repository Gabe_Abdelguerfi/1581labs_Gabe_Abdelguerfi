public class Runner
{
    public static void main(String[] args){
        
        Point2D firstPoint = new Point2D(1.0, 4.0);
        Point2D secondPoint = new Point2D(1.0, 4.0);
        
        System.out.println(firstPoint.equals(secondPoint));
        
        System.out.println(firstPoint.toString());
        System.out.println(secondPoint.toString());
        
        System.out.println();
        System.out.println( ((Object) firstPoint) .toString() );
    } // end method main
} // end class Runner