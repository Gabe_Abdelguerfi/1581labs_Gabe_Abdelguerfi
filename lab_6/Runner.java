public class Runner
{
    public static void main(String[] args)
    {
        Fraction fraction1 = new Fraction(1, 2);
        Fraction fraction2 = new Fraction(3, 7);
            
            //Prints the two Fractions
            System.out.println("Fraction1: " + fraction1.toString());
            System.out.println("Fraction2: " + fraction2.toString());
            
            //calls method add to find the sum of the two fractions and prints the sum.
            Fraction sum = fraction1.add(fraction2);
            System.out.println();
            System.out.println(fraction1 + " + " + fraction2 + " = " + sum);
            
            /* calls method subtract to find the difference of the 
            two fractions and prints the difference. */
            Fraction difference = fraction1.subtract(fraction2);
            System.out.println();
            System.out.println(fraction1 + " - " + fraction2 + " = " + difference);
            
            /* calls method multiply to find the product of the two 
            fractions and prints the product. */
            Fraction product = fraction1.multiply(fraction2);
            System.out.println();
            System.out.println(fraction1 + " * " + fraction2 + " = " + product);
            
            /* calls method divide to find the quotient of the two 
            fractions and prints the quotient. */
            Fraction quotient = fraction1.divide(fraction2);
            System.out.println();
            System.out.println(fraction1 + " / " + fraction2 + " = " + quotient);
    } //end method main
}//end class