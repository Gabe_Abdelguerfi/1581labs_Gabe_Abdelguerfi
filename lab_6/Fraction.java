/**
 * A class that models a fraction.
 * @author Gabriel Abdelguerfi
 * @revised 3 October 2016
 */
 
public class Fraction
{
    private int numerator;
    private int denominator;
    
    public Fraction( int numerator, int denominator)
    {
        this.numerator = numerator;
        this.denominator = denominator;
    } // end constructor
        
    public Fraction add(Fraction otherFract)
    {   
        //Adds two fractions
        int commonDenominator = this.denominator * otherFract.denominator;
        int newNum1 = this.numerator * otherFract.denominator;
        int newNum2 = otherFract.numerator * this.denominator;
        int sumNumerator = newNum1 + newNum2;
        Fraction sum = new Fraction(sumNumerator, commonDenominator);
        return sum;
    } //end method add
    public Fraction subtract(Fraction otherFract)
    {   
        //Subtracts two fractions
        int commonDenominator = this.denominator * otherFract.denominator;
        int newNum1 = this.numerator * otherFract.denominator;
        int newNum2 = otherFract.numerator * this.denominator;
        int sumNumerator = newNum1 - newNum2;
        Fraction difference = new Fraction(sumNumerator, commonDenominator);
        return difference;
    } //end method add
    public Fraction multiply(Fraction otherFract)
    {   
        //Multiplies two fractions.
        int prodNum = this.numerator * otherFract.numerator;
        int prodDen = this.denominator * otherFract.denominator;
        return new Fraction(prodNum, prodDen);
    } // end method multiply
    public Fraction divide(Fraction otherFract)
    {
        //Divides two fractions.
        Fraction divisorInverse = new Fraction(otherFract.denominator, otherFract.numerator);
        return this.multiply(divisorInverse);
    } // end method divide
    public String toString() 
    {
        String fractString = this.numerator + " / " + this.denominator;
        return fractString;
    } // end method toString
} // end class fraction